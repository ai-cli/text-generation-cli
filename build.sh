#!/bin/bash

export RUSTUP_TOOLCHAIN=stable
export CARGO_TARGET_DIR=target
cargo build --frozen --release --all-features

mv target/release/text-generation-cli target/release/tg-cli
