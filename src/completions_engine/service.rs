use std::string::FromUtf8Error;

use crate::completions_engine::schema::CompletionResponse;
use futures_util::StreamExt;
use once_cell::sync::Lazy;
use reqwest::Client;
use serde::Deserialize;
use serde_json::from_str;
use thiserror::Error;

use super::schema::{CompletionsRequest, Message};

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum ChatRole {
    System,
    User,
    Assistant,
}

#[derive(Debug, Deserialize, Clone)]
pub struct ChatRoles {
    system: String,
    user: String,
    assistant: String,
}

impl Default for ChatRoles {
    fn default() -> Self {
        ChatRoles {
            system: "system".to_string(),
            user: "user".to_string(),
            assistant: "assistant".to_string(),
        }
    }
}

impl ChatRoles {
    pub fn from_enum(&self, role: ChatRole) -> &str {
        match role {
            ChatRole::System => self.system.as_str(),
            ChatRole::User => self.user.as_str(),
            ChatRole::Assistant => self.assistant.as_str(),
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "kebab-case")]
pub enum ChatMode {
    Chat,
    Instruct,
    ChatInstruct,
}

impl ToString for ChatMode {
    fn to_string(&self) -> String {
        match self {
            ChatMode::Chat => "chat".to_string(),
            ChatMode::Instruct => "instruct".to_string(),
            ChatMode::ChatInstruct => "chat-instruct".to_string(),
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct ChatMessage {
    pub role: ChatRole,
    pub content: String,
}
impl ChatMessage {
    pub fn new(role: ChatRole, content: String) -> Self {
        ChatMessage { role, content }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(default, rename_all = "camelCase")]
pub struct CompletionsClientConfig {
    pub chat_completions_url: String,
    pub instruction_template: String,
    pub max_tokens: u128,
    pub temperature: f32,
    pub top_p: f32,
    pub stop: Vec<String>,
    pub chat_mode: ChatMode,
    pub chat_roles: ChatRoles,
    pub initial_chat: Option<Vec<ChatMessage>>,
    pub initiate_chat: bool,
}

static DEFAULT_COMPLETIONS_CLIENT_CONFIG: Lazy<CompletionsClientConfig> =
    Lazy::new(|| CompletionsClientConfig {
        chat_completions_url: "http://192.168.1.77:5000/v1/chat/completions".to_string(),
        instruction_template: "Alpaca".to_string(),
        max_tokens: 500,
        temperature: 0.9,
        top_p: 1.0,
        stop: vec![],
        chat_mode: ChatMode::ChatInstruct,
        chat_roles: ChatRoles::default(),
        initial_chat: None,
        initiate_chat: true,
    });

impl Default for CompletionsClientConfig {
    fn default() -> Self {
        DEFAULT_COMPLETIONS_CLIENT_CONFIG.clone()
    }
}

impl<'a> Default for &'a CompletionsClientConfig {
    fn default() -> &'a CompletionsClientConfig {
        &DEFAULT_COMPLETIONS_CLIENT_CONFIG
    }
}

#[derive(Debug, Default)]
pub struct CompletionsClient<'a> {
    pub config: &'a CompletionsClientConfig,
    pub client: Client,
}

impl<'a> CompletionsClient<'a> {
    pub fn new(completions_client_config: &'a CompletionsClientConfig) -> Self {
        Self {
            client: Client::default(),
            config: completions_client_config,
        }
    }
}

#[derive(Debug, Error)]
pub enum CompletionsClientError {
    #[error(transparent)]
    ReqwestError(#[from] reqwest::Error),
    #[error(transparent)]
    StandardStringOperation(#[from] FromUtf8Error),
    #[error(transparent)]
    Serde(#[from] serde_json::Error),
}

pub type CompletionsClientResult<T> = Result<T, CompletionsClientError>;

impl<'a> CompletionsClient<'a> {
    fn build_completions_request(&self, chat_messages: &[ChatMessage]) -> CompletionsRequest {
        CompletionsRequest {
            instruction_template: self.config.instruction_template.to_string(),
            chat_messages: chat_messages
                .iter()
                .map(|m| Message {
                    role: self.config.chat_roles.from_enum(m.role).to_string(),
                    content: m.content.to_string(),
                })
                .collect(),
            max_tokens: self.config.max_tokens,
            temperature: self.config.temperature,
            top_p: self.config.top_p,
            stop: self.config.stop.iter().map(|s| s.to_string()).collect(),
            mode: self.config.chat_mode.to_string(),
            stream: true,
        }
    }
    pub async fn prompt_chat(
        &self,
        messages: &[ChatMessage],
        on_token: impl Fn(&str),
    ) -> CompletionsClientResult<String> {
        let request_builder = self.client.post(self.config.chat_completions_url.as_str());

        let build_completions_request = self.build_completions_request(messages);

        let mut stream = request_builder
            .body(serde_json::to_string(&build_completions_request)?)
            .send()
            .await?
            .bytes_stream();

        let mut full_text = Vec::new();
        let mut buffer = Vec::new();
        while let Some(item) = stream.next().await {
            for byte in item? {
                if byte == b'\n' {
                    let text = &String::from_utf8(buffer.clone())?;
                    if text.len() > 6 {
                        let json_text = &text[6..];
                        let obj: CompletionResponse = from_str(json_text)?;
                        full_text.push(obj.choices[0].message.content.clone());
                        on_token(&obj.choices[0].message.content);
                    }

                    buffer.clear();
                } else {
                    buffer.push(byte);
                }
            }
        }

        Ok(full_text.join(""))
    }
}
