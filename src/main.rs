use chat::{Chat, MessageProviderResult};
use completions_engine::service::CompletionsClient;
use config::cli_arguments::CliArguments;
use config::model_config::ConfigLoader;
use std::io;
use std::io::{BufRead, Write};
use std::process::exit;
use structopt::StructOpt;

mod chat;
mod completions_engine;
mod config;

fn read_user_input() -> io::Result<MessageProviderResult> {
    let mut input_buf = String::new();

    print!("(Ctrl+C to exit; Enter to regenerate)> ");
    io::stdout().flush()?;
    io::stdin().lock().read_line(&mut input_buf)?;

    match input_buf.trim() {
        "" => Ok(MessageProviderResult::Regenerate),
        message => Ok(MessageProviderResult::Message(message.to_string())),
    }
}

#[tokio::main]
async fn main() {
    let CliArguments {
        config_file,
        user_prompt,
    } = CliArguments::from_args();
    match ConfigLoader::new(&config_file) {
        Ok(ConfigLoader(config)) => {
            let client = CompletionsClient::new(&config);

            if let Err(err) =
                Chat::start_loop(&client, user_prompt.to_string(), read_user_input).await
            {
                println!("Sorry, I couldn't process that: {err}");
                exit(1);
            }
        }
        Err(err) => {
            println!("Error while loading configuration file: {err}. Exiting.");
            exit(1);
        }
    }
}
