use std::io;
use std::io::Write;

use thiserror::Error;

use crate::completions_engine::service::{
    ChatMessage, ChatRole, CompletionsClient, CompletionsClientError,
};

#[derive(Debug)]
pub struct Chat {
    chat: Vec<ChatMessage>,
}

pub enum MessageProviderResult {
    Message(String),
    Regenerate,
}

#[derive(Debug, Error)]
pub enum ChatError {
    #[error(transparent)]
    CompletionsClientError(#[from] CompletionsClientError),
    #[error(transparent)]
    Io(#[from] io::Error),
}

pub type ChatResult<T> = Result<T, ChatError>;

impl<'a> Chat {
    fn new(chat: &Option<Vec<ChatMessage>>) -> Self {
        Chat {
            chat: chat.clone().unwrap_or_default(),
        }
    }

    fn add_new_message(&mut self, role: ChatRole, message: String) {
        self.chat.push(ChatMessage::new(role, message));
    }

    pub async fn start_loop(
        client: &CompletionsClient<'a>,
        first_user_message: String,
        next_message_provider: impl Fn() -> io::Result<MessageProviderResult>,
    ) -> ChatResult<()> {
        let mut this: Chat = Chat::new(&client.config.initial_chat);
        this.add_new_message(ChatRole::User, first_user_message);

        loop {
            this.add_new_message(
                ChatRole::Assistant,
                client
                    .prompt_chat(&this.chat, |token| {
                        print!("{}", token);
                        io::stdout().flush().unwrap_or_default();
                    })
                    .await?,
            );
            println!();

            if !client.config.initiate_chat {
                return Ok(());
            }

            match next_message_provider()? {
                MessageProviderResult::Message(user_message) => {
                    this.add_new_message(ChatRole::User, user_message);
                }
                MessageProviderResult::Regenerate => {
                    this.chat.pop();
                }
            }
        }
    }
}
