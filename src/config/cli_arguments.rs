use structopt::StructOpt;

pub static DEFAULT_CONFIG_FILE: &str = "config.json";

#[derive(Debug, StructOpt)]
#[structopt(
    rename_all = "kebab",
    about = "A CLI frontend for text generation APIs."
)]
pub struct CliArguments {
    #[structopt(default_value = DEFAULT_CONFIG_FILE, short, long)]
    pub config_file: String,
    #[structopt(short, long)]
    pub user_prompt: String,
}
