use etcetera::HomeDirError;
use etcetera::{choose_app_strategy, AppStrategy, AppStrategyArgs};
use serde_json;
use serde_json::from_reader;
use std::fs::File;
use std::io::BufReader;
use std::io::{self, ErrorKind};
use thiserror::Error;

use crate::completions_engine::service::CompletionsClientConfig;
use crate::config::cli_arguments::DEFAULT_CONFIG_FILE;

#[derive(Debug, Error)]
pub enum ConfigError {
    #[error(transparent)]
    PreparePath(#[from] HomeDirError),
    #[error(transparent)]
    Io(#[from] io::Error),
    #[error(transparent)]
    Serde(#[from] serde_json::Error),
}

pub type ConfigResult<T> = Result<T, ConfigError>;

#[derive(Debug)]
pub struct ConfigLoader(pub CompletionsClientConfig);

impl ConfigLoader {
    pub fn new(completions_client_config_json_filename: &str) -> ConfigResult<Self> {
        let strategy = choose_app_strategy(AppStrategyArgs {
            top_level_domain: "pt".to_string(),
            author: "ranguna".to_string(),
            app_name: "Text generation CLI".to_string(),
        })?;

        std::fs::create_dir_all(strategy.config_dir())?;

        match File::open(strategy.config_dir().join(completions_client_config_json_filename)) {
            Ok(file) => {
                let config_json_reader = BufReader::new(file);

                let config_file_contents: CompletionsClientConfig = from_reader(config_json_reader)?;

                return Ok(ConfigLoader(config_file_contents));
            }
            Err(err) if err.kind() == ErrorKind::NotFound => {
                if completions_client_config_json_filename != DEFAULT_CONFIG_FILE {
                    println!("Config file {completions_client_config_json_filename} not found.")
                }
            }
            Err(err) => println!("Error loading config file: {err}\nStarting with default values."),
        }

        Ok(ConfigLoader(CompletionsClientConfig::default()))
    }
}
